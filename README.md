# HH-4

Jira story #4

Arguments to pass to main:
```
file1.mrc+file2.mrc joinedresult.mrc
```

Validation:
```bash
copy /b /y file1.mrc+file2.mrc joinedresult.mrc
```
### For comparing, using Terminal
```bash
cat file1.mrc file2.mrc > merged.mrc && diff merged.mrc  joinedresult.mrc
```

Nothing will be prompted that means they are identical.