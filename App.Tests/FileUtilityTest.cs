using System.IO;
using System.Linq;
using Xunit;

namespace App.Tests
{
    public class FileUtilityTest
    {
        [Fact]
        public void Test__Basic_Merge()
        {
            // Arrange
            var input1 = new MemoryStream(new byte[] {1, 2, 3});
            var input2 = new MemoryStream(new byte[] {4, 5, 6});
            var output = new MemoryStream();

            // Act
            FileUtility.ConcatenateStreams(output, input1, input2);

            // Assert
            Assert.Equal(new byte[] {1, 2, 3, 4, 5, 6}, output.ToArray());
        }
        
        [Fact]
        public void Test__Many_Merge()
        {
            // Arrange
            const int count = 100;
            var inputStreams = Enumerable.Range(0, count)
                .Select(i => new MemoryStream(new[] {(byte) i}))
                .Cast<Stream>()
                .ToArray();
            var output = new MemoryStream();

            // Act
            FileUtility.ConcatenateStreams(output, inputStreams);

            // Assert
            Assert.Equal(count, output.ToArray().Length);
            Assert.Equal(Enumerable.Range(0, 100).Select(i => (byte) i).ToArray(), output.ToArray());
        }
        
        [Fact]
        public void Test__Empty_Merge()
        {
            // Arrange
            var output = new MemoryStream();

            // Act
            FileUtility.ConcatenateStreams(output);

            // Assert
            Assert.Equal(0, output.Length);
        }
    }
}