using System.IO;

namespace App
{
    public class FileUtility
    {
        /// <summary>
        /// receive the output stream and an array of input stream
        /// concatenate all inputs and spit out as a one output stream
        /// </summary>
        /// <param name="outputStream"></param>
        /// <param name="inputStreams"></param>
        public static void ConcatenateStreams(Stream outputStream, params Stream[] inputStreams)
        {
            foreach (var inputStream in inputStreams)
            {
                inputStream.CopyTo(outputStream);
            }
        }
    }
}