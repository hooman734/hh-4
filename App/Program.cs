﻿using System;
using System.IO;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace App
{
    class Program
    {
        static void Main(string[] args)
        {
            var logger = LoggerFactory.Create(x => x.AddConsole())
                .CreateLogger("Program");

            try
            {
                if (args.Length < 2)
                {
                    throw new ArgumentException($"Invalid number of arguments ({args.Length})");
                }

                if (string.IsNullOrWhiteSpace(args[0]) || string.IsNullOrWhiteSpace(args[1]))
                {
                    throw new ArgumentException("Invalid input");
                }

                var inputPaths = args[0].Split('+');
                var outputPath = args[1];

                var inputStreams = inputPaths.Select(s => new FileStream(s, FileMode.Open)).Cast<Stream>().ToArray();
                var outputStream = new FileStream(outputPath, FileMode.Append);

                FileUtility.ConcatenateStreams(outputStream, inputStreams);
            }
            catch (ArgumentNullException e)
            {
                logger.LogError(e, "Invalid Arguments");
            }
        }
    }
}